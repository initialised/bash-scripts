#!/bin/bash

main()
{
	logFile=""
	email=""
	birthDate=""
	groups=""
	sharedFolder=""
	sharedFolderGroup=""
	sharedFolder=""
	sharedFolder=""
	userCreateSuccess="0"

	file=$1

	#Checks the provided arguments are useable
	CheckArguments
	#Asks user to confirm how many users will be created
	ConfirmNumUsers
	#Creates log file, stores absolute path in $logFile
	CreateLogFile
	#Reads in file and creates users
	CreateUsersFromFile
}


CheckArguments()
{
	local providedFile

    #Check if an argument was provided
    if [[ -z "$file" ]]
      then
		echo "===ERROR!================================"
		echo "You must provide either a local file name or a URL containing the details of the users to be added as an argument"
		read -p "Enter a file or URL:  " providedFile
		file=$providedFile
    fi

	while [[ $file != 'q' && ! -f $file && $file =~ https?://.* ]] ;
      do
	  	echo "===ERROR!================================"
		echo "The argument you provided is not a valid file or URL"
		read -p "Please try again, or enter 'q' to cancel the script:  " providedFile
		file=$providedFile
	done

    #Check if the argument is a URL
    if [[ $file =~ https?://.* ]]
      then
		GetFileFromURL "$file"
	fi

}


ConfirmNumUsers()
{
	local confirm
	local numUsers
	
	local numUsers=$(wc -l < "$file")
	
	#Get user confirmation of num users to be added
	#echo "========================================="
	#read -p "There are $numUsers user entries detected in $file, would you like to continue? [y/n]  " confirm

	PromptUserConfirm "There are $numUsers user entries detected in $file, would you like to continue?"

	#If the exit status of PromptUserConfirm is not 0
	#Then the user did not confirm
	#End script
	if [[ $? != 0 ]]
	  then
		echo "Exiting..."
		exit 0
	fi

}

CreateUsersFromFile()
{
    IFS=";"

    while read email birthDate groups sharedFolder
      do
	  	#If any errors occured during creation of user is set to one
	  	userCreateSuccess="0"
		echo "Email: $email"
		echo "Birth Date: $birthDate"
		echo "Groups: $groups"
		echo "Shared Folder: $sharedFolder"

		#GetUserName creates username from email address
		#Stores username in $userName
		GetUserName "$email"
		
		#If the user does not exist, continue running the script to set up the user
		#Redirects all output from groups command to /dev/null
		if ! groups "$userName" > /dev/null 2>&1 ;
		then

			#Get user confirmation to create user
			PromptUserConfirm "Confirm creation of user $userName?"

			#If the exit status of PromptUserConfirm is 0
			#The user confirmed to create the user
			if [ $? == 0 ] ;
			  then

				#Store user password in $pass
				GetUserPass

				#Store users groups in an array
				if [ "$groups" != "" ] ;
				  then
				  	echo "Getting groups"
					GetUserGroupsArray
					echo "Done getting groups"
				fi
				
				echo "Creating user"
				CreateUser

			else

				echo "User $userName skipped"

			fi

		else

			echo "User $userName already exists and will not be edited."
			echo "User $userName already exists and will not be edited." >> $logFile
			userCreateSuccess="1"

		fi

		CheckExitStatus $userCreateSuccess "User $userName created entirely successfully" "Creating $userName encountered errors"

    done < "$file"
}


GetFileFromURL()
{
    url=$1
    wget "$url" 2> /dev/null
	CheckExitStatusLogRedirectForce $? "File at $url downloaded successfully" "File at $url could not be downloaded"
    file=$(basename "$url")
}


CreateUser()
{
	#Create the user, setting shell to bash, home directory name to their username
	useradd -d /home/"$userName" -m -s /bin/bash "$userName"
	CheckExitStatusLogRedirect $? "User $userName added" "User $userName could not be added"
	
	#Create users password, reading in echoed password from input stream
	echo "$userName":"$pass" | chpasswd -c MD5
	CheckExitStatusLogRedirect $? "User $userName password set" "User $userName password could not be set"

	#Force user to change password upon first login
	chage -d 0 "$userName"
	CheckExitStatusLogRedirect $? "User $userName password set to expire on first logon" "User $userName password could not be set to expire on first logon"

	if [ "$groups" != "" ] ;
	  then
		#For each through groups array
		for group in "${groupsArray[@]}"
		do
			#If the group is not the user's group
			if [ "$userName" != "$group" ];
			then
				echo "$group"

				#If the group does not exist, add the group
				if ! getent group "$group" > /dev/null ;
				then
					echo "$group"
					groupadd "$group"
					CheckExitStatusLogRedirect $? "Group $group added" "Group $group could not be added"
				fi
				
				usermod -a -G "$group" "$userName"
				CheckExitStatusLogRedirect $? "User $userName added to group $group" "User $userName could not be added to group $group"
			fi
		done
	fi

	#Add users shared folder
	if [ "$sharedFolder" != "" ]
	  then
	  	AddSharedFolderGroup
		AddSharedFolder
		CreateLinkToShared
	fi


	#Add alias to shutdown system
	echo "alias off='systemctl poweroff'" >> /home/$userName/.bashrc
	CheckExitStatusLogRedirect $? "Alias off created sucessfully for $userName" "Could not create alias off for $userName"

}


GetUserName()
{
	local email=$1
	local lastName
	local firstNameInitial

	if [[ $email == *"."*"@"* ]] ;
	  then
		#Store characters between . and @ in email address
		lastName=$(echo "$email" | grep -o "\\..*@")

		#Remove . at the start of lastName
		lastName=${lastName/#\./}

		#Remove @ at the end of lastName
		lastName=${lastName/%\@/}
		
		#Get the first character of the first name
		firstNameInitial=$(echo "$email" | cut -c 1)

		userName=$firstNameInitial$lastName

	elif [[ $email == *"@"* ]] ;
	  then
		userName="${email%@*}"
	else
		echo "Invalid email was provided $email"
		echo "Invalid email was provided $email" >> $logFile
		echo "Exiting..."
		exit 1
	fi
}


GetUserPass()
{
	pass=$(date --date="$birthDate" +%m%Y)
}


GetUserGroupsArray()
{
	IFS=',' read -r -a groupsArray <<< "$groups"
	for group in "${groupsArray[@]}"
	do
		echo "$group"
	done 
}


AddSharedFolderGroup()
{
	#Group name shall be name of shared folder with 'Group' appended
	sharedFolderGroup="${sharedFolder/#\//}Group"

	#If the group for the shared folder does not exist
	if ! getent group "$sharedFolderGroup" > /dev/null ;
	  then
	  	groupadd "$sharedFolderGroup"
		CheckExitStatusLogRedirect $? "Group $sharedFolderGroup added" "Group $sharedFolderGroup could not be added"
	fi
}


AddSharedFolder()
{
	#ERROR CHECK THIS
	#Create the shared folder
	#If the shared folder does not already exist, create it and assign the group as owner
	if [[ ! -d "$sharedFolder" ]] ;
	  then
		#Create the shared directory
	  	mkdir $sharedFolder
		CheckExitStatusLogRedirect $? "Successfully created directory $sharedFolder" "Creating directory $sharedFolder unsuccessful"
		#Assign user running the script as owner
		chown $SUDO_USER: $sharedFolder
		CheckExitStatusLogRedirect $? "Setting $SUDO_USER as owner of $sharedFolder successful" "Setting $SUDO_USER as owner of $sharedFolder unsuccessful"
		#Assign shared folder group as group owner for shared folder
		chown :$sharedFolderGroup $sharedFolder
		CheckExitStatusLogRedirect $? "Setting $sharedFolderGroup as group owner of $sharedFolder successful" "Setting $sharedFolderGroup as group owner of $sharedFolder unsuccessful"
		#Assign owner and group read, write, execute permissions
		#setGID = 2 so new subfiles will inherit same group as shared directory
		chmod 2770 $sharedFolder
		CheckExitStatusLogRedirect $? "Setting permissions for $sharedFolder successfull" "Setting permissions for $sharedFolder unsuccessfull"
	fi

	#Add the user to the shared folder group
	usermod -a -G "$sharedFolderGroup" "$userName"
	CheckExitStatusLogRedirect $? "User $userName added to group $sharedFolderGroup" "User $userName could not be added to group $sharedFolderGroup"
}

CreateLinkToShared()
{
	#Create link to shared folder
	local linkPath="/home/$userName/shared"
	ln -sf $sharedFolder $linkPath
	CheckExitStatusLogRedirect $? "Successfully created link $linkPath to $sharedFolder" "Creating link to $sharedFolder for $userName unsuccessful"

	#If the link was created sucessfully
	if [ $? == 0 ] ;
	  then

		#Setting user as link owner
		chown -h $userName:$userName $linkPath
		CheckExitStatusLogRedirect $? "$userName ownership set successfully for link $linkPath" "Setting $userName as owner for $linkPath unsuccessful"

		#Setting permissions for link
		chmod 700 $linkPath
		CheckExitStatusLogRedirect $? "Permissions set successfully for link $linkPath" "Setting permissions for $linkPath unsuccessful"

	fi
}


PromptUserConfirm()
{
	local confirmMessage=$1
	local confirm
	#Function returns exit status 1 unless user confirms
	local confirmStatus=1
	
	#Ask the user to confirm
	echo "========================================="
	echo "$confirmMessage [y/n]  "
	#Read in user input, do not print user input
	read -s </dev/tty confirm #Working
	#read -s confirm #Not working

	#Keep asking user for input while it is not valid
	while [[ $confirm != [yYnN]* ]] ;
	  do
		echo "Enter 'y' or 'n' to confirm/cancel"
		read -s </dev/tty confirm #Working
		#read -s confirm #Not working
	done

	#If the user confirmed
	if [[ $confirm == [yY]* ]] ;
	  then
	  	#Make the exit staus for this function 0
	  	confirmStatus=0
	fi

	return "$confirmStatus"
}


CheckExitStatus()
{
	local exitStatus="$1"
	local successMessage="$2"
	local failMessage="$3"

	#If the exit status indicates successful
	if [[ "$exitStatus" == 0 ]] ;
	  then

		echo "$successMessage"
	
	else

		echo "$failMessage"
		userCreateSuccess="1"

	fi
}

CheckExitStatusLogRedirect()
{
	local exitStatus="$1"
	local successMessage="$2"
	local failMessage="$3"

	#If the exit status indicates successful
	if [[ "$exitStatus" == 0 ]] ;
	  then
		echo "$successMessage" >> $logFile

	else
		echo "$failMessage"
		echo "$failMessage" >> $logFile
		userCreateSuccess="1"
		PromptUserConfirm "Would you like to continue?"
		if [ "$?" != 0 ]  ;
		  then
			echo "Exiting..."
			exit 1
		fi
	fi
}


CheckExitStatusLogRedirectForce()
{
	local exitStatus="$1"
	local successMessage="$2"
	local failMessage="$3"

	#If the exit status indicates successful
	if [[ "$exitStatus" == 0 ]] ;
	  then
		echo "$successMessage" >> $logFile

	else
		echo "$failMessage"
		echo "$failMessage" >> $logFile
		userCreateSuccess="1"
		echo "Exiting..."
		exit 1
	fi
}


CreateLogFile()
{
	local timeStamp=$(date +%d-%m-%Y_%H-%M-%S)
	local logDir=$(echo "$HOME/CreateUserLog")

	#If the log directory does not exist already, create it
	if [[ ! -d "$logDir" ]] ;
	  then
	  	mkdir $logDir

		#Make the user executing the script the owner of the new log file directory
		chown $SUDO_USER: $logDir
		#Give user rwx permissions
		chmod 700 $logDir
	fi

	#Store the name including path of the log file
	logFile=$(echo "$logDir/$timeStamp.log")

	#Create the logfile
	touch $logFile

	local readableTimeStamp=$(date)

	#Append header line to log file with date and time
	echo "=========================================" >> $logFile
	echo "Log file for user creation script, executed on $readableTimeStamp" >> $logFile
	echo "=========================================" >> $logFile
}

main "$1"