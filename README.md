# Bash Scripts

## README submitScript.sh

### Summary:

This script uploads a tarball archive of a directory to a remote location using SCP.
1) A tarball archive of the given directory is created in the directory the script is run from
2) The archive is uploaded to the remote location determined by the variables defined at the top of the script
3) The local copy of the tarball archive is removed


### Pre-requisites:

The user running the script must have the following permissions...
- Execute permissions to run the script
- Read and write permissions for the script if the upload destination requires editing
- Write access in the directory the script is run from
- Read access for the directory to be uploaded
- Read access for every file, directory and subfile within the directory to be uploaded

The folling variables must be correctly defined at the top of the script...
- Destination ip address
- Destination port number
- Destination user name
- Destination directory location for upload
- The name for the archive

If a password is required to access the remote location the user must enter it at the prompt.


### Insructions:

1. Ensure all pre-requisites are met

2. To run the script from it's directory use the following command:
	./submitScript.sh <absolute file path to directory for upload>
	e.g. ./submitScript.sh /home/user/forUpload
   To run the script from a directory other than the one where the script resides use the following command:
	<absolute path to script including root dir>/submitScript.sh <absolute file path to directory for upload>
	e.g. /home/user/submitScript.sh /home/user/forUpload

3. If a directory for upload was not provided you will be prompted to enter it. For best results use absolute path.
	e.g. /home/user/submitScript.sh

3. If you provided a non-existent directory you will be prompted to provide a directory again. For best results use absolute path.
	e.g. /home/user/submitScript.sh
   If instead you wish to cancel running the script, enter q here to quit
	e.g. q
4. If prompted, enter the password to login as the user at the remote location

5. Review the output, if the final output reads as below the script ran sucessfully
	Upload complete
	Exiting...
   If the script did not run successfully review the messages it displayed


