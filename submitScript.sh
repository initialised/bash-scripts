#!/bin/bash

Main() {

	file=$1
	compressedDirectoryPath=""

	#===========================================
	# Edit variables for file upload destination here:

	ipAddress="" #Destination IP or URL
	port="" #Destination port number
	destinationUser="" #Destination user
	destinationDirectory="" #Must be absolute path
	compressedFileName="" #Excluding file extension

	#===========================================

	CheckWorkingDirectoryWritable

	#Checks $file was provided when running script and is a valid directory
	CheckArguments

	CheckDirectoryReadable

	CheckDirectoryContentsReadable

	CompressDirectory
	CheckExitStatus $? "Compressed directory sucessfully created" "An error occured while compressing directory"

	UploadDirectory
	CheckExitStatus $? "Uploaded directory archive sucessfully to $ipAddress" "An error occured while uploading the directory archive"

	RemoveCompressedDirectory
	local removeExitStatus=$?
	#Let user know the location of the compressed file if it cannot be deleted, but only if it's file path variable was unaffected by the error
	if [ -z "$compressedDirectoryPath" ] ;
	  then
		CheckExitStatus $removeExitStatus "Removed compressed directory from local system sucessfully" "An error occured while removing the compressed directory from $compressedDirectoryPath"
	else
		CheckExitStatus $removeExitStatus "Removed compressed directory from local system sucessfully" "An error occured while removing the compressed directory from $pwd"
	fi

	echo "========================================="
	echo "Upload complete"
	echo "Exiting..."
}


CheckArguments()
{
	local providedDirectory

	#Check if an argument was provided
    if [[ -z "$file" ]] ;
      then
		#If a directory was not provided ask the user to provide one
		echo "========================================="
		read -p "Enter the name of the directory you wish to compress:  " providedDirectory
		file=$providedDirectory
	fi

    #Check if argument is a directory
	#Using 'q' as exit character, even though user could not submit a directory named 'q' without specifying the absolute path
	#This is better than having 'enter' as the quit choice as user is likely to hit enter before reading prompt
	#Also better than a more complicated escape sequence including illegal directory characters - too painful to type
	while [[ $file != 'q' && ! -d $file ]] ;
      do
	  	echo "========================================="
		echo "The argument you provided is not a valid directory"
		read -p "Enter the name of the directory, including file path if required. Or enter 'q' to cancel the script:  " providedDirectory
		file=$providedDirectory
	done

	#If the user opted to quit, exit the script
	if [[ "$file" == 'q' ]] ;
	  then
		echo "Exiting..."
		exit 0
	fi
}

#Checks that the user has write access in PWD so that archive can be saved there
CheckWorkingDirectoryWritable()
{
	local pwd=$(pwd)

	if [[ ! -w $pwd ]] ;
	  then
		echo "This script requires that you have write permissions in the directory you run it from"
		echo "Exiting..."
		exit 1
	fi
}

#Checks recursively that the user has read access for all files within the directory for archiving
CheckDirectoryContentsReadable()
{
	#Find any unreadable files
	if [[ $( find . ! -readable 2> /dev/null ) ]] ;
	  then
		echo "This script requires that you have read permissions for all files within the directory to be compressed"
		echo "Exiting..."
		exit 1
	fi
}

#Checks that the user has read access to directory for archive so that archive can be created
CheckDirectoryReadable()
{
	if [[ ! -r $file ]] ;
	  then
		echo "This script requires that you have read permissions for the directory to be compressed"
		echo "Exiting..."
		exit 1
	fi
}

#Creates a tarball archive of the provided directory in the PWD 
CompressDirectory()
{
	#Get the path of the directory the user is running the script
	local pwd=$(pwd)
	#Will save compressed file in PWD
	compressedDirectoryPath=$(echo "$pwd/$compressedFileName.tar.gz")
	#Ensure a file by that name does not already exist
	if [[ ! -f $compressedDirectoryPath ]]
	  then
	  	#Create the archive in the PWD
		tar -czf $compressedDirectoryPath $file
		#Check the archive was created sucessfully
		CheckExitStatus $? "Tarball archive of $file was created sucessfully" "Tarball archive of $file could not be created"
	else
		#Inform user of existing file and exit
		echo "The file $compressedDirectoryPath already exists"
		echo "Please switch directories or delete/rename the existing file"
		echo "Exiting..."
		exit 1
	fi
}

#Uploads the compressed directory to the destination as specified in variables at top of this script
UploadDirectory()
{
	#Adding line above where user will be prompted for password
	echo "========================================="
	scp -P $port $compressedDirectoryPath $destinationUser@$ipAddress:$destinationDirectory
}

#Removes the compressed directory from the local system
RemoveCompressedDirectory()
{
	rm $compressedDirectoryPath
}

#Accepts exits status as input parameter
#Outputs success message on 0 exits status
#Outputs failure message and exits script if the exit status was not 0
CheckExitStatus()
{
	local exitStatus="$1"
	local successMessage="$2"
	local failMessage="$3"

	#If the exit status indicates successful
	if [[ "$exitStatus" == 0 ]] ;
	  then

		echo "$successMessage"
	
	else

		echo "$failMessage"
		echo "Exiting..."
		exit 1

	fi
}

#Call Main to begin script
Main "$1"